# less7.1

containers(node_exporter+prometheus+alertmanager) в docker-compose

ЗАДАНИЕ

1)Настроить prometheus+alertmanager на отправку уведомлений об уменьшении свободного дискового пространства ниже определенного порога (назначить самостоятельно). 
  Уведомления отправлять по электронной почте. Предоставить конфиги prometheus, alertmanager, текст полученного письма с заголовками

ОТВЕТ НА ЗАДАНИЕ

Используем doker-compose.yam для поднятия контейнеров node-exporter, x-prometheus (c конфигом /monitor/prometheus.yml и правилами alerts /monitor/rules/alertes.yml), alertmanager (c конфигом /alertmanager/alertmanager.yml ).

Алерт будет приходить на почту gmail когда выражение будет true [ (100 - (node_filesystem_avail_bytes/node_filesystem_size_bytes * 100)) > 50]. В данном случае выбрал больше 50%.
 
